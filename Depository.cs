﻿using _1_biblio_system;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_biblio_forms
{
    public class Depository
    {
        List<Book> free_books;

        public Depository()
        {
            free_books = new List<Book>();
        }
        List<Book> GetList()
        {
            //– вернуть весь список(без удаления)
            return new List<Book>(free_books);
        }

        Book GiveBook(int id)
        {
            //– вернуть(c удалением из списка) книгу по id.Если таковая отсутствует, вернуть null
            foreach (Book b in free_books)
            {
                if (b.id == id)
                {
                    free_books.Remove(b);
                    return b;
                }
            }

            return null;
        }

        void AcceptBook(Book bk)
        {
            //– внести книгу в список
            free_books.Add(bk);
        }
        void AcceptBooks(List<Book> lbk)
        {
            free_books.AddRange(lbk);
        }
    }
}
